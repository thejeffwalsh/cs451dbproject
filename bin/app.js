//imports
const express = require('express');
const request = require('request');
const http = require('http');
const https = require('https');
const staticServe = require('serve-static');
const fs = require('fs');
const bodyParser = require('body-parser');
const xml2json = require('xml2json');
const path = require('path');
const Pool = require('pg').Pool;
const pool = new Pool({
	user: 'node',
	password: 'password',
	database: 'milestone1db',
});

//server setup
const app = express();

const port = 1337;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true}));

app.get('/cities', (req, res) => {
	pool.connect(
		pool.query(`SELECT DISTINCT city
		FROM business
		WHERE state = '${req.query['state']}  '
		ORDER BY city;`, (err, result) => {
			if (err) {
				console.error('frick', err.stack)
			}
			else {
				res.json(result);
			}
		})
	);
});

app.get('/names', (req, res) => {
	pool.connect(
		pool.query(`SELECT name
		FROM business
		WHERE city= '${req.query['city']}' AND state= '${req.query['state']}  '
		ORDER BY name; ;`, (err, result) => {
			if (err) {
				console.error('frick', err.stack)
			}
			else {
				res.json(result);
			}
		})
	);
});

app.get('/states', (req, res) => {
	pool.connect(
		pool.query(`SELECT DISTINCT state
		FROM business
		ORDER BY state;`, (err, result) => {
			if (err) {
				console.error('frick', err.stack)
			}
			else {
				res.json(result);
			}
		})
	);
});

//app.get('/', (req, res) => {res.send("Hello World");});

app.listen(port, () => {console.log(`app ${port}`)});
//serving static page
app.use(staticServe('./public/html', {'index': ['index.html', 'index.html']}));



