var express = require('express');
var fs = require('fs');
var readline = require('readline');

var interface = readline.createInterface({input: fs.createReadStream('yelp_review.JSON')});


interface.on('line', function(line) {
	var jString = line.toString();
	var jObj = JSON.parse(jString);
	console.log(jObj.review_id);
	console.log(jObj.user_id);
	console.log(jObj.business_id);
	console.log(jObj.stars);
	console.log(jObj.date);
	console.log(jObj.text);
	console.log('\b');
});
