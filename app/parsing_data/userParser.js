var fs = require('fs');
var readline = require('readline');

var interface = readline.createInterface({input: fs.createReadStream('yelp_user.JSON')});

interface.on('line', function(line) {
	var jString = line.toString();
	var jObj = JSON.parse(jString);

	console.log(jObj.average_stars);
	console.log(jObj.cool);
	console.log(jObj.fans);
	console.log(jObj.friends);
	console.log(jObj.funny);
	console.log(jObj.name);
	console.log(jObj.review_count);
	console.log(jObj.useful);
	console.log(jObj.user_id);
	console.log(jObj.yelping_since);
});
